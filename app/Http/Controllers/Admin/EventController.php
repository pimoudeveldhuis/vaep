<?php
/**
* Admin\EventController
*
* Controller responsible for management of events
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EventController extends Controller {
    public function render(Request $request, $action = null, $id = null) {
        if($action !== null) {
            if($action === 'detail') {
                return self::render_event_detail($request, $id);
            } elseif($action === 'create') {
                return self::render_event_create($request);
            } elseif($action === 'update') {
                return self::render_event_update($request, $id);
            } elseif($action === 'delete') {
                return self::render_event_delete($request, $id);
            }
        } else {
            return self::render_event_list($request);
        }
    }
    
    public function render_event_list(Request $request) {
        if(parent::access('event', $request) || parent::access('schedule', $request)) {
            $events = DB::table('event')->get();

            return View('/templates/'.env('TEMPLATE').'/admin/event/list', ['events' => $events]);
        }
    }
    
    public function render_event_detail(Request $request, $id) {
        $event = DB::table('event')->where('id', $id)->first();
        
        return View('/templates/'.env('TEMPLATE').'/admin/event/detail', ['event' => $event]);
    }
    
    public function render_event_create(Request $request) {
        if(parent::access('event', $request)) {
            if($request->method() === 'POST') {
                $error = array();

                if($request->input('name') === '')
                    $error[] = 'Missing event name';

                if($request->input('start') === '')
                    $error[] = 'Missing date of start';

                if($request->input('end') === '')
                    $error[] = 'Missing date of end';

                if(count($error) > 0) {
                    return View('/templates/'.env('TEMPLATE').'/admin/event/form', ['input_name' => $request->input('name'), 'input_start' => $request->input('start'), 'input_end' => $request->input('end'), 'input_pilot_schedule' => $request->input('pilot_schedule'), 'input_pilot_tracker' => $request->input('pilot_tracker'), 'input_atc_schedule' => $request->input('atc_schedule'), 'input_atc_tracker' => $request->input('atc_tracker'), 'error' => $error]);
                } else {
                    DB::table('event')->insert(['name' => $request->input('name'), 'start' => new Datetime($request->input('start')), 'end' => new Datetime($request->input('end')), 'pilot_schedule' => (($request->input('pilot_schedule') != null) ? $request->input('pilot_schedule') : '0'), 'pilot_tracker' => (($request->input('pilot_tracker') != null) ? $request->input('pilot_tracker') : '0'), 'atc_schedule' => (($request->input('atc_schedule') != null) ? $request->input('atc_schedule') : '0'), 'atc_tracker' => (($request->input('atc_tracker') != null) ? $request->input('atc_tracker') : '0')]);

                    return redirect('/admin/event/details/'.DB::getPdo()->lastInsertId().'/'.$request->input('name'));
                }
            }

            return View('/templates/'.env('TEMPLATE').'/admin/event/form');
        }
    }
    
    public function render_event_update(Request $request, $id) {
        if(parent::access('event', $request)) {
            $event = DB::table('event')->where('id', $id)->first();
            if($event !== null) {
                if($request->method() === 'POST') {
                    $error = array();

                    if($request->input('name') === '')
                        $error[] = 'Missing event name';

                    if($request->input('start') === '')
                        $error[] = 'Missing date of start';

                    if($request->input('end') === '')
                        $error[] = 'Missing date of end';

                    if(count($error) > 0) {
                        return View('/templates/'.env('TEMPLATE').'/admin/event/form', ['input_name' => $request->input('name'), 'input_start' => $request->input('start'), 'input_end' => $request->input('end'), 'input_pilot_schedule' => $request->input('pilot_schedule'), 'input_pilot_tracker' => $request->input('pilot_tracker'), 'input_atc_schedule' => $request->input('atc_schedule'), 'input_atc_tracker' => $request->input('atc_tracker'), 'error' => $error]);
                    } else {
                        //Todo: check for changes in schedule (either pilot or ATC) and add or remove the schedule data accordingly
                        
                        DB::table('event')->where('id', $id)->update(['name' => $request->input('name'), 'start' => new Datetime($request->input('start')), 'end' => new Datetime($request->input('end')), 'pilot_schedule' => (($request->input('pilot_schedule') != null) ? $request->input('pilot_schedule') : '0'), 'pilot_tracker' => (($request->input('pilot_tracker') != null) ? $request->input('pilot_tracker') : '0'), 'atc_schedule' => (($request->input('atc_schedule') != null) ? $request->input('atc_schedule') : '0'), 'atc_tracker' => (($request->input('atc_tracker') != null) ? $request->input('atc_tracker') : '0')]);

                        return redirect('/admin/event/detail/'.$id.'/'.$request->input('name'));
                    }
                }

                return View('/templates/'.env('TEMPLATE').'/admin/event/form', ['input_name' => $event->name, 'input_start' => $event->start, 'input_end' => $event->end, 'input_pilot_schedule' => $event->pilot_schedule, 'input_pilot_tracker' => $event->pilot_tracker, 'input_atc_schedule' => $event->atc_schedule, 'input_atc_tracker' => $event->atc_tracker]);
            } else { exit('Error, ATC Station ID not found'); }
        }
    }
    
    public function render_event_delete(Request $request, $id) {
        if(parent::access('event', $request)) {
            $event = DB::table('event')->where('id', $id)->first();
            if($event !== null) {
                DB::table('event')->where('id', $event->id)->delete();
                return redirect('/admin/event');
            } else {
                exit('Error, ATC Station ID not found');
            }
        }
    }
}