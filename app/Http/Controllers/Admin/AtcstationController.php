<?php
/**
* Admin\AtcstationController
*
* Controller responsible for managing the ATC Stations
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AtcstationController extends Controller {
    public function render(Request $request, $action = null, $id = null) {
        if(parent::access('position', $request)) {
            if($action !== null) {
                if($action === 'create') {
                    return self::render_atcstation_create($request);
                } elseif($action === 'update') {
                    return self::render_atcstation_update($request, $id);
                } elseif($action === 'delete') {
                    return self::render_atcstation_delete($request, $id);
                }
            } else {
                return self::render_atcstation_list($request);
            }
        }
    }
    
    public function render_atcstation_list(Request $request) {
        return View('/templates/'.env('TEMPLATE').'/admin/atcstation/list', ['atc_stations' => DB::table('atc_station')->orderBy('order', 'asc')->get()]);
    }
    
    public function render_atcstation_create(Request $request) {
        if($request->method() === 'POST') {
            $error = array();
            
            if($request->input('ICAO') === '')
                $error[] = 'Missing ICAO';
            
            if($request->input('frequency') === '')
                $error[] = 'Missing frequency';
            
            
            if(count($error) > 0) {
                return View('/templates/'.env('TEMPLATE').'/admin/atcstation/form', ['input_ICAO' => $request->input('ICAO'), 'input_frequency' => $request->input('frequency'), 'input_category' => $request->input('category'), 'input_fra' => $request->input('fra'), 'error' => $error]);
            } else {
                DB::table('atc_station')->insert(['category' => ($request->input('category') !== '') ? $request->input('category') : null, 'ICAO' => $request->input('ICAO'), 'frequency' => $request->input('frequency'), 'fra' => $request->input('fra'), 'order' => '999']);
                
                return redirect('/admin/atcstation');
            }
        }
        
        return View('/templates/'.env('TEMPLATE').'/admin/atcstation/form');
    }
    
    public function render_atcstation_update(Request $request, $id) {
        $atc_station = DB::table('atc_station')->where('id', $id)->first();
        if($atc_station !== null) {
            if($request->method() === 'POST') {
                $error = array();

                if($request->input('ICAO') === '')
                    $error[] = 'Missing ICAO';

                if($request->input('frequency') === '')
                    $error[] = 'Missing frequency';


                if(count($error) > 0) {
                    return View('/templates/'.env('TEMPLATE').'/admin/atcstation/form', ['input_ICAO' => $request->input('ICAO'), 'input_frequency' => $request->input('frequency'), 'input_category' => $request->input('category'), 'input_fra' => $request->input('fra'), 'error' => $error]);
                } else {
                    DB::table('atc_station')->where('id', $id)->update(['ICAO' => $request->input('ICAO'), 'frequency' => $request->input('frequency'), 'category' => $request->input('category'), 'fra' => $request->input('fra')]);

                    return redirect('/admin/atcstation');
                }
            }
        
            return View('/templates/'.env('TEMPLATE').'/admin/atcstation/form', ['input_ICAO' => $atc_station->ICAO, 'input_frequency' => $atc_station->frequency, 'input_category' => $atc_station->category, 'input_fra' => $atc_station->fra]);
        } else { exit('Error, ATC Station ID not found'); }
    }
    
    public function render_atcstation_delete(Request $request, $id) {
        $atc_station = DB::table('atc_station')->where('id', $id)->first();
        if($atc_station !== null) {
            DB::table('atc_station')->where('id', $atc_station->id)->delete();
            return redirect('/admin/atcstation');
        } else {
            exit('Error, ATC Station ID not found');
        }
    }
}