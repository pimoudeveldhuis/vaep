<?php
/**
* Admin\StaffController
*
* Controller responsible for management of staff accounts
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StaffController extends Controller {
    public function render(Request $request, $action = null, $id = null) {
        if(parent::access('staff', $request)) {
            if(env('LOGIN') === 'ivao') {
                return self::render_ivao_staff($request);
            } else {
                if($action !== null) {

                } else {
                    return self::render_event_list($request);
                }
            }
        }
    }
    
    public function render_ivao_staff(Request $request) {
        if($request->method() === 'POST') {
            $error = array();
                        
            if($request->input('event') !== '')
                $_tmp_json['event'] = (count(explode(";", $request->input('event'))) >= count(explode(",", $request->input('event')))) ? explode(";", $request->input('event')) : explode(",", $request->input('event'));
            
            if($request->input('schedule') !== '')
                $_tmp_json['schedule'] = (count(explode(";", $request->input('schedule'))) >= count(explode(",", $request->input('schedule')))) ? explode(";", $request->input('schedule')) : explode(",", $request->input('schedule'));
            
            if($request->input('position') !== '')
                $_tmp_json['position'] = (count(explode(";", $request->input('position'))) >= count(explode(",", $request->input('position')))) ? explode(";", $request->input('position')) : explode(",", $request->input('position'));
            
            if($request->input('staff') !== '')
                $_tmp_json['staff'] = (count(explode(";", $request->input('staff'))) >= count(explode(",", $request->input('staff')))) ? explode(";", $request->input('staff')) : explode(",", $request->input('staff'));
            else
                $_tmp_json['staff'] = [parent::user()->id];
            
            
            $conf_access_ivao = DB::table('conf')->where('key', 'access_ivao')->first();
            if($conf_access_ivao !== null)
                DB::table('conf')->where(['key' => 'access_ivao'])->update(['value' => json_encode($_tmp_json)]);
            else
                DB::table('conf')->insert(['key' => 'access_ivao', 'value' => json_encode($_tmp_json)]);
                
            return redirect('/admin/staff');
        }
        
        $conf_access_ivao = DB::table('conf')->where('key', 'access_ivao')->first();
        if($conf_access_ivao !== null) {
            $_tmp_json = json_decode($conf_access_ivao->value, 1);
            
            if(array_key_exists('event', $_tmp_json))
                $_tmp_event = implode(";", $_tmp_json['event']);
            else
                $_tmp_event = null;
            
            if(array_key_exists('schedule', $_tmp_json))
                $_tmp_schedule = implode(";", $_tmp_json['schedule']);
            else
                $_tmp_schedule = null;
            
            if(array_key_exists('position', $_tmp_json))
                $_tmp_position = implode(";", $_tmp_json['position']);
            
            if(array_key_exists('staff', $_tmp_json))
                $_tmp_staff = implode(";", $_tmp_json['staff']);
            else
                $_tmp_staff = null;
                        
            return View('/templates/'.env('TEMPLATE').'/admin/staff/ivao', ['event' => (isset($_tmp_event) ? $_tmp_event : null), 'schedule' => (isset($_tmp_schedule) ? $_tmp_schedule : null), 'position' => (isset($_tmp_position) ? $_tmp_position : null), 'staff' => (isset($_tmp_staff) ? $_tmp_staff : null)]);
        }
        
        return View('/templates/'.env('TEMPLATE').'/admin/staff/ivao');
    }
}