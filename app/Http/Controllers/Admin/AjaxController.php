<?php
/**
* AjaxController
*
* Controller responsible for handling the AJAX events
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers\Admin;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AjaxController extends Controller {
    public function render(Request $request, $action = null) {
        if($action !== null) {
            if($action === 'atc_station_list_sort') {
                return self::atc_station_list_sort($request);
            } elseif($action === 'atc_station_categories') {
                return self::atc_station_categories($request);
            }
        }
        
        return null;
    }
    
    public function atc_station_list_sort(Request $request) {
        if(parent::access('atcstation', $request)) {
            if($request->input('order') !== null && count($request->input('order') > 0)) {
                $order = 1;

                foreach($request->input('order') AS $ID) {
                    DB::table('atc_station')->where('id', $ID)->update(['order' => $order]);
                    $order++;
                }
            }
        }
        
        return null;
    }
    
    public function atc_station_categories(Request $request) {
        if(parent::access('atcstation', $request)) {
            return DB::table('atc_station')->whereNotNull('category')->groupBy('category')->pluck('category');
        }
        
        return null;
    }
}