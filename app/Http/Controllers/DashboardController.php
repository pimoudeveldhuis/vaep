<?php
/**
* DashboardController
*
* Controller responsible for the primary Dashboard showing after login
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller {
    public function render(Request $request) {
        if(parent::access('*', $request)) {
            return view('templates/default/user/dashboard');
        }
    }
}