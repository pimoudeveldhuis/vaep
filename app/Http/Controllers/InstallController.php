<?php
/**
* InstallController
*
* Controller responsible for installing and upgrading the system
*
* @author Pim Oude Veldhuis <pim@odvh.nl>
* @license MIT
*/
namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class InstallController extends Controller {
    public function render(Request $request) {
        var_dump($request->path());
        if($request->path() === 'install')
            self::install();
    }
    
    public function install() {
        /* access */
        DB::statement("DROP TABLE IF EXISTS `access`");
        DB::statement("CREATE TABLE `access` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(64) NOT NULL DEFAULT '', `description` tinytext NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8");
        DB::statement("INSERT INTO `access` (`id`, `name`, `description`) VALUES (1, 'staff', 'Can edit access rights for members and groups'), (2, 'position', 'Can create, edit and remove ATC Positions for events'), (3, 'event', 'Can create, edit, archive and remove events'), (4, 'schedule', 'Can view the schedule applications and has the right to change the schedule');");

        /* atc_station */
        DB::statement("DROP TABLE IF EXISTS `atc_station`");
        DB::statement("CREATE TABLE `atc_station` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `category` varchar(32) DEFAULT NULL, `ICAO` varchar(32) NOT NULL DEFAULT '', `frequency` varchar(16) NOT NULL, `fra` varchar(8) DEFAULT NULL, `order` int(5) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        /* conf */
        DB::statement("DROP TABLE IF EXISTS `conf`");
        DB::statement("CREATE TABLE `conf` (`key` varchar(64) NOT NULL DEFAULT '', `value` varchar(512) NOT NULL DEFAULT '', PRIMARY KEY (`key`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        DB::statement("INSERT INTO `conf` (`key`, `value`) VALUES ('version', '1');");
        
        if(env('LOGIN') === 'ivao')
            DB::statement("INSERT INTO `conf` (`key`, `value`) VALUES ('access_ivao', '{\"event\":[\"".env('ADMIN')."\"],\"schedule\":[\"".env('ADMIN')."\"],\"position\":[\"".env('ADMIN')."\"],\"staff\":[\"".env('ADMIN')."\"]}');");
        
        /* event */
        DB::statement("DROP TABLE IF EXISTS `event`");
        DB::statement("CREATE TABLE `event` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(128) NOT NULL DEFAULT '', `start` datetime NOT NULL, `end` datetime NOT NULL, `pilot_schedule` tinyint(1) NOT NULL DEFAULT '0', `pilot_tracker` tinyint(1) NOT NULL DEFAULT '0', `atc_schedule` tinyint(1) NOT NULL DEFAULT '0', `atc_tracker` tinyint(1) NOT NULL DEFAULT '0', `status` varchar(16) NOT NULL DEFAULT 'PREPARE', PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        /* event_status */
        DB::statement("DROP TABLE IF EXISTS `event_status`");
        DB::statement("CREATE TABLE `event_status` (`id` varchar(16) NOT NULL DEFAULT '', `order` int(1) unsigned NOT NULL, `visible` int(1) DEFAULT NULL, `book_atc` int(1) DEFAULT NULL, `book_pilot` int(1) DEFAULT NULL, `tracker` int(1) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        DB::statement("INSERT INTO `event_status` (`id`, `order`, `visible`, `book_atc`, `book_pilot`, `tracker`) VALUES ('PREPARE', 1, NULL, NULL, NULL, NULL), ('PUBLIC', 2, 1, 1, 1, NULL), ('LOCKED', 3, 1, NULL, 1, NULL), ('ACTIVE', 4, 1, NULL, 1, 1), ('ARCHIVED', 5, NULL, NULL, NULL, NULL);");

        /* group */
        DB::statement("DROP TABLE IF EXISTS `group`");

        /* group_access */
        DB::statement("DROP TABLE IF EXISTS `group_access`");
        
        /* user_group */
        DB::statement("DROP TABLE IF EXISTS `user_group`");
        
        if(env('LOGIN') !== 'ivao') {
            /* group */
            DB::statement("CREATE TABLE `group` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `name` varchar(32) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

            /* group_access */
            DB::statement("CREATE TABLE `group_access` (`group_id` int(11) unsigned NOT NULL, `access_id` int(11) unsigned NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

            /* user_group */
            DB::statement("CREATE TABLE `user_group` (`user_id` int(6) unsigned NOT NULL, `group_id` int(11) unsigned NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        }
        
        /* session */
        DB::statement("DROP TABLE IF EXISTS `session`");
        DB::statement("CREATE TABLE `session` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `user` int(6) unsigned NOT NULL, `datetime` datetime NOT NULL, `ipaddr` varchar(128) NOT NULL DEFAULT '', `browserinfo` tinytext NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        /* user */
        DB::statement("DROP TABLE IF EXISTS `user`");
        DB::statement("CREATE TABLE `user` (`id` int(6) unsigned NOT NULL, `name` varchar(128) NOT NULL DEFAULT '', `password` varchar(256) DEFAULT NULL, `atc_rating` int(4) DEFAULT NULL, `pilot_rating` int(4) DEFAULT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        /* user_access */
        DB::statement("DROP TABLE IF EXISTS `user_access`");
        DB::statement("CREATE TABLE `user_access` (`user_id` int(6) unsigned NOT NULL, `access_id` int(11) unsigned NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        
        DB::statement("INSERT INTO `user_access` (`user_id`, `access_id`) VALUES (".env('ADMIN').", 1), (".env('ADMIN').", 2), (".env('ADMIN').", 3), (".env('ADMIN').", 4);");
        
        if(env('APP_DEBUG') === TRUE) {
            DB::statement("INSERT INTO `user` (`id`, `name`, `password`, `atc_rating`, `pilot_rating`) VALUES (100000, 'Testaccount', NULL, 9, 9);");
            
            DB::statement("INSERT INTO `event` (`id`, `name`, `start`, `end`, `pilot_schedule`, `pilot_tracker`, `atc_schedule`, `atc_tracker`, `status`) VALUES (1, 'Name', '2017-01-05 14:56:49', '2017-01-05 14:56:49', 0, 1, 1, 1, 'PREPARE'), (2, 'Event Name 2', '2017-01-05 15:21:00', '2017-01-05 15:21:00', 1, 1, 1, 1, 'PREPARE'), (3, 'Event Name 3', '2017-01-05 15:22:55', '2017-01-05 15:22:55', 0, 1, 0, 0, 'PUBLIC');");
            
            DB::statement("INSERT INTO `atc_station` (`id`, `category`, `ICAO`, `frequency`, `fra`, `order`) VALUES (1, 'Schiphol', 'EHAM_A_TWR', '111.111', 'AS3', 1), (2, 'Schiphol', 'EHAM_W_APP', '222.222', 'ACP', 2), (5, 'Schiphol', 'EHAM_DEL', '555.555', 'AS1', 3), (3, 'Rotterdam', 'EHRD_TWR', '333.333', 'AS3', 4), (4, 'Rotterdam', 'EHRD_DEL', '444.444', 'AS1', 5);");
        }
    }
}