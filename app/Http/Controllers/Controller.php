<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
    * Access method
    *
    * Method that checks whether the user has access
    *
    * @author Pim Oude Veldhuis <pim@odvh.nl>
    * @license MIT
    */
    public static function access($access, Request $request) {
        if(self::user() === null) {
            if(env('APP_DEBUG') === true) {
                DB::table('session')->insert(['user' => 100000, 'datetime' => new DateTime(), 'ipaddr' => $_SERVER['REMOTE_ADDR'], 'browserinfo' => $_SERVER['HTTP_USER_AGENT']]);
                $request->session()->put('sess', DB::getPdo()->lastInsertId());
                
                return redirect($request->path());
            } else {
                //todo: login redirect
            }
        } else {
            if($access === '*') {
                return true;
            } else {
                $access_id = DB::table('access')->where('name', $access)->value('id');

                if($access_id) {
                    if(DB::table('user_access')->where([['access_id', $access_id], ['user_id', self::user()->id]])->first() !== null) {
                        return true;
                    } else {
                        if(DB::table('user_group')->where('user_id', self::user()->id)->whereIn('group_id', DB::table('group_access')->where('access_id', $access_id)->pluck('group_id')->toArray())->first()) {
                            return true;
                        }
                    }
                }
            }
            
            //todo: login redirect
            exit;
        }
        
        return false;
    }
    
    public static function sess() {
        
    }
    
    public static function user() {
        return DB::table('user')->where(['id' => DB::table('session')->where(['id' => \Session::get('sess')])->value('user')])->first();
    }
    
    public static function user_name() {
        
    }
    
    public static function user_atc_rating() {
        
    }
    
    public static function user_pilot_rating() {
        
    }
}
