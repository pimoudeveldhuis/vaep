<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'DashboardController@render');

Route::get('/install', 'InstallController@render');
Route::get('/update', 'InstallController@render');

Route::match('get', '/admin/ajax/{action?}','Admin\AjaxController@render');

Route::get('/admin', 'DashboardController@render');

Route::match(['get', 'post'], '/admin/event/{action}/{id}/{event?}','Admin\EventController@render');
Route::match(['get', 'post'], '/admin/event/{action?}','Admin\EventController@render');

Route::match(['get', 'post'], '/admin/atcstation/{action}/{id}/{station?}','Admin\AtcstationController@render');
Route::match(['get', 'post'], '/admin/atcstation/{action?}','Admin\AtcstationController@render');

Route::match(['get', 'post'], '/admin/staff/{action}/{id}','Admin\StaffController@render');
Route::match(['get', 'post'], '/admin/staff/{action?}','Admin\StaffController@render');