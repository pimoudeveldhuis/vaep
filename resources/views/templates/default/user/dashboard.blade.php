@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
    </ol>
@endsection

@section('content')
    <div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Event</th>
                <th>Type</th>
                <th>Status</th>
                <th class="text-center">Pilot Schedule</th>
                <th class="text-center">Pilot Tracking</th>
                <th class="text-center">ATC Schedule</th>
                <th class="text-center">ATC Tracking</th>
                <th class="text-right">Date</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Schiphol Fly-in</td>
                <td>Fly-in</td>
                <td>Preparation</td>
                <td class="text-center"><i class="fa fa-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-right">12-01-2017 12:00 UTC (till 19:00 UTC)</td>
            </tr>
            <tr>
                <td>Online Weekend</td>
                <td>Other</td>
                <td>Public</td>
                <td class="text-center"><i class="fa fa-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-right">19-01-2017 10:00 UTC (till 20-01-2017 18:00 UTC)</td>
            </tr>
            <tr>
                <td>Online Weekend</td>
                <td>Other</td>
                <td>Archived</td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-center"><i class="fa fa-check-square-o"></i></td>
                <td class="text-right">19-01-2016 10:00 UTC (till 20-01-2016 18:00 UTC)</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection