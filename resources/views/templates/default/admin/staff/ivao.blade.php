@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li class="active">Staff</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <form class="form-horizontal" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group">
                <label class="col-md-2 control-label" for="textinput">Event Management</label>  
                <div class="col-md-4"><input name="event" type="text" placeholder="XX-AOC" class="form-control input-md" value="{{ $event or '' }}"></div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label" for="textinput">Schedule Management</label>  
                <div class="col-md-4"><input name="schedule" type="text" placeholder="XX-AOC" class="form-control input-md" value="{{ $schedule or '' }}"></div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label" for="textinput">Position Management</label>  
                <div class="col-md-4"><input name="position" type="text" placeholder="XX-AOC" class="form-control input-md" value="{{ $position or '' }}"></div>
            </div>
            
            <div class="form-group">
                <label class="col-md-2 control-label" for="textinput">Staff Management</label>  
                <div class="col-md-4"><input name="staff" type="text" placeholder="XX-AOC" class="form-control input-md" value="{{ $staff or '' }}"></div>
            </div>
            
            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <button class="btn btn-success">Save Staff Positions</button> <a class="btn btn-danger" href="/admin/staff">Cancel</a>
                </div>
            </div>
        </form>
    </div>
@endsection