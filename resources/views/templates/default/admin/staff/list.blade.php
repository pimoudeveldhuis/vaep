@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li class="active">Staff</li>
    </ol>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped sortable">
            
        </table>
    </div>
@endsection