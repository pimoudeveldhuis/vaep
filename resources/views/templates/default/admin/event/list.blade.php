@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li class="active">Events</li>
    </ol>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <th width="60px;"><a href="/admin/event/create"><i class="fa fa-plus"></i></a></th>
                <th>Name</th>
                <th class="text-center">Status</th>
                <th class="text-center">Pilot Schedule</th>
                <th class="text-center">Pilot Tracker</th>
                <th class="text-center">ATC Schedule</th>
                <th class="text-center">ATC Tracker</th>
                <th class="text-center">Date start</th>
                <th class="text-center">Date end</th>
            </thead>
            
            @if(count($events) > 0)
                <tbody>
                    @foreach($events AS $event)
                        <tr>
                            <td>
                                <a href="/admin/event/detail/{{ $event->id }}/{{ $event->name }}"><i class="fa fa-info"></i></a>
                                <a href="/admin/event/update/{{ $event->id }}/{{ $event->name }}"><i class="fa fa-pencil"></i></a>
                                <a href="/admin/event/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a>
                            </td>
                            <td>{{ $event->name }}</td>
                            <td class="text-center">{{ $event->status }}</td>
                            <td class="text-center"><i class="fa fa{{ $event->pilot_schedule == 1 ? '-check' : '' }}-square-o"></td>
                            <td class="text-center"><i class="fa fa{{ $event->pilot_tracker == 1 ? '-check' : '' }}-square-o"></td>
                            <td class="text-center"><i class="fa fa{{ $event->atc_schedule == 1 ? '-check' : '' }}-square-o"></td>
                            <td class="text-center"><i class="fa fa{{ $event->atc_tracker == 1 ? '-check' : '' }}-square-o"></td>
                            <td class="text-center">{{ $event->start }}</td>
                            <td class="text-center">{{ $event->end }}</td>
                        </tr>
                    @endforeach
                </tbody>
            @endif
        </table>
    </div>
@endsection