@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li><a href="/admin/event">Events</a></li>
        <li class="active">Details</li>
    </ol>
@endsection

@section('content')
    <h1 class="text-center">{{ $event->name }}</h1>
    
    <hr />

    <div class="row">
        <div class="col-md-6 text-center"><strong>{{ $event->status }}</strong><br /><br /><a href="/admin/event/status/" class="btn btn-success">Publish event</a></div>
        <div class="col-md-6 text-center"><strong>From:</strong><br />{{ $event->start }}<br /><br /><strong>Till:</strong><br />{{ $event->end }}</div>
    </div>

    <hr />
    
    <div class="row">
        <div class="col-md-3 text-center" style="color: @if($event->pilot_schedule == 1) green @else red @endif">Pilot Schedule</div>
        <div class="col-md-3 text-center" style="color: @if($event->pilot_tracker == 1) green @else red @endif">Pilot Tracker</div>
        <div class="col-md-3 text-center" style="color: @if($event->atc_schedule == 1) green @else red @endif">ATC Schedule</div>
        <div class="col-md-3 text-center" style="color: @if($event->atc_tracker == 1) green @else red @endif">ATC Tracker</div>
    </div>

    <hr />

    <h2 class="text-center" style="margin-top: 50px;">Traffic Control</h2>
    <div style="overflow: scroll; height: 300px;">
        <table class="table">
            <thead>
                <th style="width: 30px;"><a href="/admin/timetable/create/{{ $event->id }}/{{ $event->name }}"><i class="fa fa-plus"></i></a></th>
                <th>Station</th>
                <th>Timetable</th>
            </thead>
            <tbody>
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/EHAA_TWR/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>EHAM_A_TWR</td>
                    <td>
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }} <br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}<br />
                        <a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a> {{ $event->start }} - {{ $event->end }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <hr />
    <h2 class="text-center" style="margin-top: 50px;">Flights</h2>
    <div style="overflow: scroll; height: 300px;">
        <table class="table">
            <thead>
                <th style="width: 30px;"><a href="/admin/flights/create/{{ $event->id }}/{{ $event->name }}"><i class="fa fa-plus"></i></a></th>
                <th>CALLSIGN</th>
                <th>Departure</th>
                <th>Arrival</th>
                <th>Departure</th>
                <th>Arrival</th>
                <th>Status</th>
            </thead>
            <tbody>
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td>OPEN</td>
                </tr>
                
                <tr>
                    <td><a href="/admin/timetable/delete/{{ $event->id }}/{{ $event->name }}" onclick="return confirm('Are you sure you want to delete this event? This cannot be undone!')"><i class="fa fa-trash"></i></a></td>
                    <td>SUP6</td>
                    <td>EHAM - E7</td>
                    <td>EGLL</td>
                    <td>{{ $event->start }}</td>
                    <td>{{ $event->end }}</td>
                    <td style="color: green">BOOKED</td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection