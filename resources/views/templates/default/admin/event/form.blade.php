@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li><a href="/admin/event">Events</a></li>
        <li class="active">Event Form</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(!empty($error))
            <div class="form-group">
                <div class="col-md-4 col-md-offset-1">
                    <div class="alert alert-danger">
                        The following errors have occured:
                        
                        <ul>
                            @foreach($error AS $msg)
                                <li>{{ $msg }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">Name</label>  
            <div class="col-md-4"><input name="name" type="text" placeholder="Schiphol RFE" class="form-control input-md" value="{{ $input_name or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-1 control-label" for="checkboxes">Event options</label>
            <div class="col-md-4">
                <div class="checkbox"><label for="checkboxes-0"><input type="checkbox" name="pilot_schedule" id="checkboxes-0" value="1" {{ isset($input_pilot_schedule) ? ($input_pilot_schedule == '1' ? ' checked="checked"' : '') : '' }}>Pilot Schedule</label></div>
                <div class="checkbox"><label for="checkboxes-1"><input type="checkbox" name="pilot_tracker" id="checkboxes-1" value="1" {{ isset($input_pilot_tracker) ? ($input_pilot_tracker == '1' ? ' checked="checked"' : '') : '' }}>Pilot Tracker</label></div>
                <div class="checkbox"><label for="checkboxes-2"><input type="checkbox" name="atc_schedule" id="checkboxes-2" value="1" {{ isset($input_atc_schedule) ? ($input_atc_schedule == '1' ? ' checked="checked"' : '') : '' }}>ATC Schedule</label></div>
                <div class="checkbox"><label for="checkboxes-3"><input type="checkbox" name="atc_tracker" id="checkboxes-3" value="1" {{ isset($input_atc_tracker) ? ($input_atc_tracker == '1' ? ' checked="checked"' : '') : '' }}>ATC Tracker</label></div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">Date Start</label>  
            <div class="col-md-4"><input name="start" type="text" placeholder="18-10-2017 14:00" class="form-control input-md" value="{{ $input_start or '' }}"></div>
        </div>
        
        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">Date End</label>  
            <div class="col-md-4"><input name="end" type="text" placeholder="20-10-2017 12:00" class="form-control input-md" value="{{ $input_end or '' }}"></div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-1">
                <button class="btn btn-success">Prepare Event</button> <a class="btn btn-danger" href="/admin/event">Cancel</a>
            </div>
        </div>
    </form>
@endsection