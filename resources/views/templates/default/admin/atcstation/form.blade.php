@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li><a href="/admin/atcstation">ATC Stations</a></li>
        <li class="active">ATC Station Form</li>
    </ol>
@endsection

@section('content')
    <form class="form-horizontal" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
        @if(!empty($error))
            <div class="form-group">
                <div class="col-md-4 col-md-offset-1">
                    <div class="alert alert-danger">
                        The following errors have occured:
                        
                        <ul>
                            @foreach($error AS $msg)
                                <li>{{ $msg }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        
        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">ICAO</label>  
            <div class="col-md-4"><input name="ICAO" type="text" placeholder="EHAM_A_TWR" class="form-control input-md" value="{{ $input_ICAO or '' }}"></div>
        </div>

        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">Category tag</label>  
            <div class="col-md-4"><input id="inputCategory" name="category" type="text" placeholder="Schiphol" class="form-control input-md" value="{{ $input_category or '' }}"></div>
        </div>

        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">Frequency</label>  
            <div class="col-md-4"><input name="frequency" type="text" placeholder="122.800" class="form-control input-md" value="{{ $input_frequency or '' }}"></div>
        </div>

        <div class="form-group">
            <label class="col-md-1 control-label" for="textinput">FRA</label>  
            <div class="col-md-4">
                <select name="fra" class="form-control">
                    <option value="AS1" {{ isset($input_fra) ? ($input_fra == 'AS1' ? 'selected' : '') : '' }}>AS1</option>
                    <option value="AS2" {{ isset($input_fra) ? ($input_fra == 'AS2' ? 'selected' : '') : '' }}>AS2</option>
                    <option value="AS3" {{ isset($input_fra) ? ($input_fra == 'AS3' ? 'selected' : '') : '' }}>AS3</option>
                    <option value="ADC" {{ isset($input_fra) ? ($input_fra == 'ADC' ? 'selected' : '') : '' }}>ADC</option>
                    <option value="APC" {{ isset($input_fra) ? ($input_fra == 'APC' ? 'selected' : '') : '' }}>APC</option>
                    <option value="ACC" {{ isset($input_fra) ? ($input_fra == 'ACC' ? 'selected' : '') : '' }}>ACC</option>
                    <option value="SEC" {{ isset($input_fra) ? ($input_fra == 'SEC' ? 'selected' : '') : '' }}>SEC</option>
                    <option value="SAI" {{ isset($input_fra) ? ($input_fra == 'SAI' ? 'selected' : '') : '' }}>SAI</option>
                    <option value="CAI" {{ isset($input_fra) ? ($input_fra == 'CAI' ? 'selected' : '') : '' }}>CAI</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-4 col-md-offset-1">
                <button class="btn btn-success">Save ATC Station</button> <a class="btn btn-danger" href="/admin/atcstation">Cancel</a>
            </div>
        </div>
    </form>

    <script type="text/javascript">
        $.ajax({
            method: "GET",
            url: "/admin/ajax/atc_station_categories",
            success: function(callback) {
                console.log(callback);
                
                $( "#inputCategory" ).autocomplete({ source: callback });
            }
        });
        
    </script>
@endsection