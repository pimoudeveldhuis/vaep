@extends('templates/default/layout')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><i class="fa fa-cog"></i> <a href="/admin">Administrator</a></li>
        <li class="active">ATC Stations</li>
    </ol>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-striped sortable">
            <thead>
                <th width="60px;"><a href="/admin/atcstation/create"><i class="fa fa-plus"></i></a></th>
                <th>Position</th>
                <th>Category</th>
                <th>Frequency</th>
            </thead>
            @if(count($atc_stations) > 0)
                <tbody>
                    @foreach($atc_stations AS $atc_station)
                        <tr station="{{ $atc_station->id }}">
                            <td><a href="/admin/atcstation/update/{{ $atc_station->id }}/{{ $atc_station->ICAO }}"><i class="fa fa-pencil"></i></a> <a href="/admin/atcstation/delete/{{ $atc_station->id }}/{{ $atc_station->ICAO }}" onclick="return confirm('Are you sure you want to delete this ATC station?')"><i class="fa fa-trash"></i></a></td>
                            <td>{{ $atc_station->ICAO }}</td>
                            <td>{{ $atc_station->category or '' }}</td>
                            <td>{{ $atc_station->frequency }}</td>
                        </tr>
                    @endforeach
                </tbody>
            @endif
        </table>
    </div>
    
    <script>
        var sortableTableHelper = function(e, ui) { ui.children().each(function() { $(this).width($(this).width()); }); return ui; };
        $(".sortable tbody").sortable({
            helper: sortableTableHelper,
            stop: function (event, ui) {
                var atc_station_order = [];
                $.each($(this).find('tr'), function (index, value) {
                    atc_station_order.push($(this).attr('station'));
                });
                
                $.ajax({
                    method: "GET",
                    url: "/admin/ajax/atc_station_list_sort",
                    data: { order: atc_station_order }
                });
            }
        }).disableSelection();
    </script>
@endsection